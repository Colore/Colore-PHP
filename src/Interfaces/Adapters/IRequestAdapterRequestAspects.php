<?php

namespace Colore\Interfaces\Adapters;

interface IRequestAdapterRequestAspects {
    public function getRequestArgument($requestArgumentName);
}
